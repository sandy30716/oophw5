import java.applet.Applet;
import java.awt.*;
import java.io.*;
import javax.imageio.*;
 
class CSThread extends Thread {
	public static CarSim CS ;
	CSThread(){CS = new CarSim();}
	public void run(){
		try{
		CS.Simulate();}
		catch(InterruptedException e){}
	}
} 
public class JavaAppletDemo extends Applet implements Runnable{
	Image image1,image2,image3,image4,image5,image6,image7;
	Thread runner; 
	int x;	
	public void start() { 
		if (runner == null) { 
			runner = new Thread(this); 
			runner.start(); 
		} 
	} 
	public void stop() { 
		if (runner != null) { 
			runner.stop(); 
			runner = null; 
		} 
	} 
	public void run() { 
		CSThread CSthread = new CSThread();
		CSthread.start();
		while (true) {
			repaint(); 
			try { Thread.sleep(500); } 
			catch (InterruptedException e) { }
		} 
	} 
    public void paint(Graphics g) {
		int i;
			ClassLoader classLoader = this.getClass().getClassLoader(); 
			String car1 = "img/car2.png";
			String car2 = "img/car3.png";
			String car3 = "img/car4.png";
			String car4 = "img/car5.png";
			String car5 = "img/car6.png";
			String car6 = "img/car7.png";
			String car7 = "img/car8.png";
			java.net.URL image1URL = classLoader.getResource(car1);
			java.net.URL image2URL = classLoader.getResource(car2);
			java.net.URL image3URL = classLoader.getResource(car3);
			java.net.URL image4URL = classLoader.getResource(car4);
			java.net.URL image5URL = classLoader.getResource(car5);
			java.net.URL image6URL = classLoader.getResource(car6);
			java.net.URL image7URL = classLoader.getResource(car7);
			try {image1 = ImageIO.read(image1URL);}
			catch(Exception e){}
			try {image2 = ImageIO.read(image2URL);}
			catch(Exception e){}
			try {image3 = ImageIO.read(image3URL);}
			catch(Exception e){}
			try {image4 = ImageIO.read(image4URL);}
			catch(Exception e){}
			try {image5 = ImageIO.read(image5URL);}
			catch(Exception e){}
			try {image6 = ImageIO.read(image6URL);}
			catch(Exception e){}
			try {image7 = ImageIO.read(image7URL);}
			catch(Exception e){}
		
		for(i=0;i<CSThread.CS.in_way;i++){
			if(CSThread.CS.Info[i].position>0 && CSThread.CS.Info[i].position < CSThread.CS.Len){
				if(i%7==0){g.drawImage(image1,(CSThread.CS.Info[i].position-50),30,null);}
				else if(i%7==1){g.drawImage(image2,(CSThread.CS.Info[i].position-50),30,null);}
				else if(i%7==2){g.drawImage(image3,(CSThread.CS.Info[i].position-50),30,null);}
				else if(i%7==3){g.drawImage(image4,(CSThread.CS.Info[i].position-50),30,null);}
				else if(i%7==4){g.drawImage(image5,(CSThread.CS.Info[i].position-50),30,null);}
				else if(i%7==5){g.drawImage(image6,(CSThread.CS.Info[i].position-50),30,null);}
				else if(i%7==6){g.drawImage(image7,(CSThread.CS.Info[i].position-50),30,null);}
				}
		}
    }
}