import java.lang.*;

class CarInfo{
	public int num;
	public int position;
	public int speed;
	CarInfo(int n){num=n;position=0;}
}
/**
	CarThread : Each car is a thread. 
**/
class CarThread extends Thread{
	public Object k;
	int num;
	CarThread(int n,Object key){num=n;k=key;}
	public void run(){
		while(CarSim.Info[num].position<CarSim.Len){
			synchronized(k){
				
				if(num==0){CarSim.Info[num].speed=50;}
				else{
					if((CarSim.Info[num-1].position-CarSim.Info[num].position)>150){
						set_speed(CarSim.Info[num-1].position-CarSim.Info[num].position);
					}
					else
						set_speed(0);
				}
				set_position();
				if(CarSim.Info[num].position>=CarSim.Len){CarSim.Info[num].position=2*CarSim.Len;}
				printRoad();
			}
			try{
				Thread.sleep(500);}
			catch(InterruptedException e){}
		}
	}
	public void set_speed(int distance){
		int tmp=50;
		if(tmp>distance/2){
			tmp=distance/2;
		}
		CarSim.Info[num].speed=tmp;
	}
	public void set_position(){CarSim.Info[num].position+=CarSim.Info[num].speed;}
	public void printRoad(){
		int i;
		StringBuffer road = new StringBuffer();
		for(i=0;i<CarSim.Len;i++)
			road.append('.');
		for(i=0;i<CarSim.in_way;i++){
			if(CarSim.Info[i].position>0 && CarSim.Info[i].position < CarSim.Len){
				System.out.println(i);	
				road.setCharAt(CarSim.Info[i].position-1, 'x');
			}
		}
		System.out.println(road);
	}
}
/**
	CarSim: (Car Simulation) run the cars depends on the number of cars
	# of cars:20
**/
public class CarSim{
	public static Object key=new Object();
	public static int Len=1100;
	public static int CarNum=20;
	public static int in_way=0;
	public static CarThread[] Cars = new CarThread[CarNum];
	public static CarInfo[] Info = new CarInfo[CarNum];
	public static void Simulate() throws InterruptedException{
		for(int i=0;i<CarNum;i++){
			Cars[i]= new CarThread(i,key);
			Info[i]=new CarInfo(i);
			Thread.sleep(500);
			Cars[i].start();
			if(in_way<CarNum){in_way++;}			
		}
	}
}